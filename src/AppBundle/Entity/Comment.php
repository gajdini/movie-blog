<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/1/2019
 * Time: 4:45 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity
 * @ORM\Table(name="comment")
 */
class Comment
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timeDone;

    /**
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * Many comments belong to one Review. This is the owning side.
     * @ManyToOne(targetEntity="AppBundle\Entity\Review", inversedBy="comments")
     * @JoinColumn(name="review_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $review;

    /**
     * Many comments belong to one User. This is the owning side.
     * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="comments")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTimeDone()
    {
        return $this->timeDone;
    }

    /**
     * @param mixed $timeDone
     */
    public function setTimeDone($timeDone)
    {
        $this->timeDone = $timeDone;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param mixed $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}