<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/7/2019
 * Time: 9:38 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AboutRepository")
 * @ORM\Table(name="about")
 */
class About
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="about.about.not_blank")
     * @Assert\Length(
     *     max="255",
     *     min="2",
     *     minMessage="about.about.minMessage",
     *     maxMessage="about.about.maxMessage")
     */
    protected $about;

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     */
    public function setAbout($about)
    {
        $this->about = $about;
    }

}