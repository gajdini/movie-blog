<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/14/2019
 * Time: 9:39 AM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{

    public function findAll()
    {
        return $this->createQueryBuilder('cat')
            ->orderBy('cat.name','ASC')
            ->getQuery()
            ->execute();
    }

}