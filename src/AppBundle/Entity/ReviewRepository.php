<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2019
 * Time: 9:59 AM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ReviewRepository extends EntityRepository
{
    public function paginate($dql, $page = 1, $limit=5)
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }


    public function getReviewsAccToComments($currentPage=1,$limit=5){

        $query= $this->createQueryBuilder('rev')
            ->leftJoin('rev.comments','com')
            ->addSelect('count(com) as cnt')
            ->andWhere('rev.deletedAt IS NULL')
            ->groupBy('rev.id')
            ->orderBy('cnt','DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;
    }

    public function getReviewsAccToLikes($currentPage=1,$limit=5){

        $query= $this->createQueryBuilder('rev')
            ->leftJoin('rev.likedBy','likes')
            ->addSelect('count(likes) as cnt')
            ->andWhere('rev.deletedAt IS NULL')
            ->groupBy('rev.id')
            ->orderBy('cnt','DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;
    }

    public function getReviews($currentPage=1,$limit=5){

        $query= $this->createQueryBuilder('rev')
            ->orderBy('rev.id')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;
    }

    public function getReviewsAccToTime($currentPage=1,$limit=5){

        $query=$this->createQueryBuilder('rev')
            ->andWhere('rev.deletedAt IS NULL')
            ->orderBy('rev.timePosted','DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;

    }

    public function getReviewsAccToModerator($moderator,$currentPage=1,$limit=5){
        $query= $this->createQueryBuilder('rev')
            ->innerJoin('rev.moderator','mod')
            ->andWhere('mod.username=:i')
            ->andWhere('rev.deletedAt IS NULL')
            ->setParameter('i',$moderator)
            ->orderBy('rev.timePosted','DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;
    }

    public function getMonths(){
        $sql="select monthname(time_posted) as month,year(time_posted) as year,count(id) as cnt
                from review
                where review.deleted_at IS NULL
                group by monthname(time_posted),year(time_posted)
                order by time_posted DESC
                limit 10";

        $conn=$this->getEntityManager()->getConnection();

        $smtm=$conn->prepare($sql);

        $smtm->execute();

        return $smtm->fetchAll();
    }

    public function getReviewsAccToMonthYear($month,$year,$currentPage=1,$limit=5){

        $query= $this->createQueryBuilder('rev')
            ->andWhere('MONTHNAME(rev.timePosted) = :m')
            ->andWhere('YEAR(rev.timePosted)=:y')
            ->andWhere('rev.deletedAt IS NULL')
            ->setParameter('m',$month)
            ->setParameter('y',$year)
            ->orderBy('rev.timePosted','DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;
    }

    public function getReviewsAccToCategory($category,$currentPage=1,$limit=5){

        $query= $this->createQueryBuilder('rev')
            ->innerJoin('rev.categories','cat')
            ->andWhere('cat.name=:i')
            ->andWhere('rev.deletedAt IS NULL')
            ->setParameter('i',$category)
            ->orderBy('rev.timePosted','DESC')
            ->getQuery();

        $paginator = $this->paginate($query,$currentPage,$limit);

        return $paginator;
    }

    public function getReviewsFromSearch($key,$currentPage=1,$limit=5){
        $query=$this->createQueryBuilder('rev')
            ->leftJoin('rev.moderator', 'mod')
            ->leftJoin('rev.categories', 'cat')
            ->orWhere('rev.title LIKE :i')
            ->orWhere('rev.text LIKE :i')
            ->orWhere('mod.username LIKE :i')
            ->orWhere('cat.name LIKE :i')
            ->andWhere('rev.deletedAt IS NULL')
            ->setParameter('i','%'.$key.'%')
            ->orderBy('rev.timePosted','DESC')
            ->getQuery();

        $paginator = $this->paginate($query,$currentPage,$limit);

        return $paginator;

    }

    public function deleteLike($r,$u){
        $sql="DELETE FROM likes WHERE review_id=:r AND user_id=:u";

        $conn=$this->getEntityManager()->getConnection();

        $smtm=$conn->prepare($sql);

        $smtm->bindParam(':r', $r);
        $smtm->bindParam(':u', $u);

        return $smtm->execute();
    }

    public function getReviewsOfFollowing($user,$currentPage=1,$limit=5){

        $query= $this->createQueryBuilder('rev')
            ->innerJoin('AppBundle:User','u','WITH','u.id=rev.moderator')
            ->innerJoin('u.following','f')
            ->andWhere(':i MEMBER OF u.following')
            ->andWhere('rev.deletedAt IS NULL')
            ->groupBy('rev.id')
            ->orderBy('rev.timePosted','DESC')
            ->setParameter('i',$user)
            ->getQuery();

        $paginator = $this->paginate($query,$currentPage,$limit);

        return $paginator;

//        $conn=$this->getEntityManager()->getConnection();
//
//        $sql="SELECT review.*,user.username
//            FROM review
//            INNER JOIN (user,follow)
//            ON (user.id=review.moderator_id
//            AND follow.moderator_id=review.moderator_id)
//            and follow.follower_id=$user
//            ORDER BY review.time_posted DESC";
//
//        $smtm=$conn->prepare($sql);
//
//        $smtm->execute();
//
//        return $smtm->fetchAll();

    }

}