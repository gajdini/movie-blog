<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/26/2019
 * Time: 9:36 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{

    public function getMessages($u1, $u2)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where($qb->expr()->andX(
                $qb->expr()->eq('m.sender', ':i'),
                $qb->expr()->eq('m.receiver', ':j')
            ));
            $qb->orWhere($qb->expr()->andX(
                $qb->expr()->eq('m.receiver', ':i'),
                $qb->expr()->eq('m.sender', ':j')
            ))
            ->setParameter('i', $u1)
            ->setParameter('j', $u2)
            ->orderBy('m.sentAt', 'ASC');

        return $qb->getQuery()->execute();
    }

    public function getNewMessages($u1, $u2)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.receiver=:i')
            ->andWhere('m.sender=:j')
            ->andWhere('m.seen=0')
            ->setParameter('i', $u1)
            ->setParameter('j', $u2)
            ->orderBy('m.sentAt', 'ASC')
            ->getQuery()
            ->execute();
    }


}