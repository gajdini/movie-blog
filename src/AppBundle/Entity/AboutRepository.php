<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/7/2019
 * Time: 9:45 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AboutRepository extends EntityRepository
{

    public function getAbout(){
        $qb= $this->createQueryBuilder('a')
            ->getQuery()
            ->getOneOrNullResult();

        return $qb;
    }

}