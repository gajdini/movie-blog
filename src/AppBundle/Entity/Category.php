<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/1/2019
 * Time: 4:29 PM
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Column(type="string",unique=true)
     * @Assert\NotBlank(message="category.name.not_blank")
     * @Assert\Regex(
     *      pattern     = "/^[a-z\-&\s\/]+$/i",
     *     htmlPattern = "^[a-zA-Z\-&\s\/]+$",
     *     message="category.name.regex"
     * )
     * @Assert\Length(
     *      max = 25,
     *      maxMessage = "category.name.maxMessage"
     * )
     */
    private $name;

    /**
     * Many Categories belong to Many Reviews.
     * @ManyToMany(targetEntity="AppBundle\Entity\Review", mappedBy="categories")
     */
    private $reviews;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param mixed $reviews
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }


}