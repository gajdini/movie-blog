<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/1/2019
 * Time: 4:24 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ReviewRepository")
 * @ORM\Table(name="review")
 */
class Review
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->likedBy = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="review.title.not_blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 150,
     *      minMessage = "review.title.minMessage",
     *      maxMessage = "review.title.maxMessage"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="review.text.not_blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 65535,
     *      minMessage = "review.content.minMessage",
     *      maxMessage = "review.content.maxMessage"
     * )
     */
    private $text;

    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Regex(pattern="/^https:\/\/[a-zA-Z0-9?=&.\/]+$/",
     *     message="review.link.pattern")
     */
    private $link;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $deletedAt;


    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/png","image/jpeg","image/jpg"},
     *     mimeTypesMessage = "Please upload a valid image"
     * )
     */
    private $img;

    /**
     * Many Reviews have Many Categories.
     * @ManyToMany(targetEntity="AppBundle\Entity\Category", inversedBy="reviews")
     * @JoinTable(name="review_category")
     */
    private $categories;

    /**
     * Many Reviews are liked by  Many Users.
     * @ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="likes")
     * @JoinTable(name="likes")
     */
    private $likedBy;


    /**
     * One review has many comments. This is the inverse side.
     * @OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="review")
     */
    private $comments;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="reviews")
     * @JoinColumn(name="moderator_id", referencedColumnName="id")
     */
    private $moderator; //reference to moderator that posted the review

    /**
     * @ORM\Column(type="datetime")
     */
    private $timePosted;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories[] = $categories;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getModerator()
    {
        return $this->moderator;
    }

    /**
     * @param mixed $moderator
     */
    public function setModerator($moderator)
    {
        $this->moderator = $moderator;
    }

    /**
     * @return mixed
     */
    public function getTimePosted()
    {
        return $this->timePosted;
    }

    /**
     * @param mixed $timePosted
     */
    public function setTimePosted($timePosted)
    {
        $this->timePosted = $timePosted;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return mixed
     */
    public function getLikedBy()
    {
        return $this->likedBy;
    }

    /**
     * @param mixed $likedBy
     */
    public function setLikedBy($likedBy): void
    {
        $this->likedBy[] = $likedBy;
    }

}