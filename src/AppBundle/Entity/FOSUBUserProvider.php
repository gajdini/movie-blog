<?php

namespace AppBundle\Entity;

use AppBundle\Controller\AdminController;
use FOS\UserBundle\Model\UserManagerInterface;
use http\Env\Response;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use phpDocumentor\Reflection\Types\Parent_;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;


// Source: https://gist.github.com/danvbe/4476697

class FOSUBUserProvider extends BaseClass
{
    private $session;

    public function __construct(UserManagerInterface $userManager, array $properties,Session $session)
    {
        parent::__construct($userManager, $properties);
        $this->session=$session;
    }

    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);

        $username = $response->getUsername();

// On connect, retrieve the access token and the user id
        $service = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';

// Disconnect previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

// Connect using the current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $this->userManager->updateUser($user);
    }

    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $data = $response->getResponse();
        $username = $response->getUsername();
        $name=$response->getFirstName();
        $surname=$response->getLastName();
        $email = $response->getEmail() ? $response->getEmail() : $username;
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));

// If the user is new
        if (null === $user) {

            try{
                $service = $response->getResourceOwner()->getName();
                $setter = 'set' . ucfirst($service);
                $setter_id = $setter . 'Id';
                $setter_token = $setter . 'AccessToken';
// create new user here
                $user = $this->userManager->createUser();
                $user->$setter_id($username);
                $user->$setter_token($response->getAccessToken());

                $user->setUsername($this->generateRandomUsername($username, $response->getResourceOwner()->getName()));
                $user->setEmail($email);
                $user->setPassword($username);
                $user->setEnabled(true);
                $user->setFirstName($name);
                $user->setLastName($surname);
                $this->userManager->updateUser($user);
            }
            catch (\Exception $e){
                $this->session->getFlashBag()->add('danger', 'There already exists an account with this email!');
                return;
            }

            return $user;
        }

        if($user->getDeletedAt()!=null)
            throw new CustomUserMessageAuthenticationException('Account is disabled by admin.');

// If the user exists, use the HWIOAuth
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

// Update the access token
        $user->$setter($response->getAccessToken());

        return $user;
    }

    /**
     * Generates a random username with the given
     * e.g 12345_github, 12345_facebook
     *
     * @param string $username
     * @param type $serviceName
     * @return type
     */
    private function generateRandomUsername($username, $serviceName)
    {
        if (!$username) {
            $username = "user" . uniqid((rand()), true) . $serviceName;
        }

        return $username . "_" . $serviceName;
    }
}
