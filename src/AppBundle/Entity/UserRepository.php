<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/4/2019
 * Time: 2:44 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UserRepository extends EntityRepository
{
    public function getModerators(){
        return $this->createQueryBuilder('u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->setParameter('roles', '%"ROLE_MODERATOR"%')
            ->getQuery()->execute();
    }

    public function getUsersFromSearch($key,$currentPage=1,$limit=5){
        $query=$this->createQueryBuilder('u')
            ->orWhere('u.username LIKE :i')
            ->orWhere('u.firstName LIKE :i')
            ->orWhere('u.lastName LIKE :i')
            ->setParameter('i','%'.$key.'%')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;

    }

    public function getAll($currentPage=1,$limit=5)
    {
        $query= $this->createQueryBuilder('u')
            ->leftJoin('u.messagesSent','messagesSent')
            ->addSelect('SUM(CASE WHEN messagesSent.seen = 0 THEN 1 ELSE 0 END) as cnt')
            ->groupBy('u.id')
            ->orderBy('u.id','ASC')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage,$limit);

        return $paginator;

    }

    public function getModeratorByUsername($username){
        return $this->createQueryBuilder('u') // Change this to the name of your bundle and the name of your mapped user Entity
        ->andWhere('u.username=:i')
            ->setParameter('i', $username)
            ->getQuery()->getOneOrNullResult();
    }

    public function checkIfFollow($user,$mod){
        $sql="select follow.moderator_id,follow.follower_id
                from follow,user
                where follow.follower_id=:u
                and user.id=follow.moderator_id
                and user.username=:mod";

        $conn=$this->getEntityManager()->getConnection();

        $smtm=$conn->prepare($sql);
        $smtm->bindParam(':u', $user);
        $smtm->bindParam(':mod', $mod);

        $smtm->execute();

        return $smtm->fetchAll();
    }

    public function checkIfLiked($user,$rev){
        $sql="select likes.*
                from likes,user
                where likes.user_id=:u
                and likes.review_id=:r
                and user.id=:u";

        $conn=$this->getEntityManager()->getConnection();

        $smtm=$conn->prepare($sql);
        $smtm->bindParam(':u', $user);
        $smtm->bindParam(':r', $rev);
        $smtm->execute();

        return $smtm->fetchAll();
    }


    public function deleteFollow($f,$m){
        $sql="DELETE FROM follow WHERE follower_id=:f AND moderator_id=:m";

        $conn=$this->getEntityManager()->getConnection();

        $smtm=$conn->prepare($sql);
        $smtm->bindParam(':f', $f);
        $smtm->bindParam(':m', $m);

        return $smtm->execute();
    }

    public function checkIfSubbed($email,$mod){
        $sql="select subscription.id
                from subscription,user
                where subscription.email=:email
                and user.id=subscription.moderator_id
                and user.username=:username";

        $conn=$this->getEntityManager()->getConnection();

        $smtm=$conn->prepare($sql);

        $smtm->bindParam(':email', $email);
        $smtm->bindParam(':username', $mod);

        $smtm->execute();

        return $smtm->fetchAll();
    }

    public function paginate($dql, $page = 1, $limit=5)
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }

}