<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/1/2019
 * Time: 12:35 PM
 */

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as UserAssert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->myFollowers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->following = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
        $this->likes= new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @UserAssert\ContainsOnlyLetters
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     * @UserAssert\ContainsOnlyLetters
     * @ORM\Column(type="string")
     */
    private $lastName;


    /**
     * @ORM\Column(type="boolean")
     */
    private $blocked=false;

    /**
     * Many Users like Many Reviews.
     * @ManyToMany(targetEntity="AppBundle\Entity\Review", mappedBy="likedBy")
     */
    private $likes;

    /**
     * One user makes many comments. This is the inverse side.
     * @OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="user")
     */
    private $comments;

    /**
     * One user sends many messages. This is the inverse side.
     * @OneToMany(targetEntity="AppBundle\Entity\Message", mappedBy="sender")
     */
    private $messagesSent;

    /**
     * One user receives many messages. This is the inverse side.
     * @OneToMany(targetEntity="AppBundle\Entity\Message", mappedBy="receiver")
     */
    private $messagesReceived;

    /**
     * One user has many subscriptions. This is the inverse side.
     * @OneToMany(targetEntity="AppBundle\Entity\Subscription", mappedBy="moderator")
     */
    private $subscriptions;

    /**
     * One user writes many reviews. This is the inverse side.
     * @OneToMany(targetEntity="AppBundle\Entity\Review", mappedBy="moderator")
     */
    private $reviews;

    /**
     * Many Users are followed by many Users.
     * @ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="following")
     */
    private $myFollowers;

    /**
     * Many Users follow many Users.
     * @ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="myFollowers")
     * @JoinTable(name="follow",
     *      joinColumns={@JoinColumn(name="moderator_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="follower_id", referencedColumnName="id")}
     *      )
     */
    private $following;

    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Length(
     *     max="255",
     *     maxMessage="user.bio.maxMessage")
     */
    private $bio;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $google_id;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $google_access_token;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $deletedAt;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @param mixed $subscriptions
     */
    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }

    /**
     * @return mixed
     */
    public function getMyFollowers()
    {
        return $this->myFollowers;
    }

    /**
     * @param mixed $myFollowers
     */
    public function setMyFollowers($myFollowers)
    {
        $this->myFollowers[] = $myFollowers;
    }

    /**
     * @return mixed
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @param mixed $following
     */
    public function setFollowing($following)
    {
        $this->following[] = $following;
    }

    /**
     * @return mixed
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param mixed $reviews
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param mixed $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param mixed $blocked
     */
    public function setBlocked($blocked): void
    {
        $this->blocked = $blocked;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * @param mixed $google_id
     */
    public function setGoogleId($google_id): void
    {
        $this->google_id = $google_id;
    }

    /**
     * @return mixed
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

    /**
     * @param mixed $google_access_token
     */
    public function setGoogleAccessToken($google_access_token): void
    {
        $this->google_access_token = $google_access_token;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes): void
    {
        $this->likes[] = $likes;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return mixed
     */
    public function getMessagesSent()
    {
        return $this->messagesSent;
    }

    /**
     * @param mixed $messagesSent
     */
    public function setMessagesSent($messagesSent): void
    {
        $this->messagesSent[] = $messagesSent;
    }

    /**
     * @return mixed
     */
    public function getMessagesReceived()
    {
        return $this->messagesReceived;
    }

    /**
     * @param mixed $messagesReceived
     */
    public function setMessagesReceived($messagesReceived): void
    {
        $this->messagesReceived[] = $messagesReceived;
    }


}