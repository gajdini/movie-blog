<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/6/2019
 * Time: 11:06 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{

    /**
     * @Route("/categories", name="categories")
     */
    public function showCategories(Request $request){
        $repository = $this->getDoctrine()->getRepository(Category::class);

        $categories=$repository->findAll();

        $category=new Category();

        $form = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($category);
                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'Category was added!'
                );

            } catch (\Exception $e) {
                $this->addFlash(
                    'danger',
                    'Category already exists!'
                );
            }

            return $this->redirect('/categories');
        }

        return $this->render('Category/category.html.twig',[
            'categories'=>$categories,
            'form'=>$form->createView(),
        ]);

    }


    }