<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/3/2019
 * Time: 8:36 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\About;
use AppBundle\Entity\User;
use AppBundle\Form\AboutFormType;
use AppBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;


class HomeController extends Controller
{
    /**
     * @Route("/home", name="home")
     */
    public function showHomepage()
    {

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $review = $reviewRepository->getReviewsAccToComments();

        $mostRecent = $reviewRepository->getReviewsAccToTime();

        $user = $this->getUser();

        if ($user != null)
            $followRev = $reviewRepository->getReviewsOfFollowing($this->getUser());
        else
            $followRev = null;

        return $this->render('Home/home.html.twig', [
            'reviews' => $review,
            'recent' => $mostRecent,
            'followRev' => $followRev,
        ]);

    }


    /**
     * @Route("/reviews/time/{month}/{year}", name="monthReviews")
     */
    public function showMonthReviews(Request $request, $month, $year)
    {
        $page = $request->get('page');
        if ($page == null)
            $page = 1;

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $reviews = $reviewRepository->getReviewsAccToMonthYear($month, $year, $page);

        $limit = 5;
        $maxPages = ceil($reviews->count() / $limit);

        $thisPage = $page;

        return $this->render('Review/monthReviews.html.twig', [
            'reviews' => $reviews,
            'maxPages' => $maxPages,
            'thisPage' => $thisPage
        ]);
    }

    /**
     * @Route("/searchResults", name="searchResults")
     */
    public function search(Request $request)
    {
        $key = $request->get('key');

        $page = $request->get('page');
        if ($page == null)
            $page = 1;

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $reviews = $reviewRepository->getReviewsFromSearch($key, $page);

        $limit = 5;

        $maxPages = ceil($reviews->count() / $limit);

        $thisPage = $page;

        return $this->render("Home/search.html.twig", [
            'reviews' => $reviews,
            'thisPage' => $thisPage,
            'maxPages' => $maxPages,
            'key' => $key
        ]);
    }


    /**
     * @Route("/about", name="about")
     */
    public function showAbout(Request $request)
    {
        $userRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:User');

        $moderators = $userRepository->getModerators();

        $about = $this->getDoctrine()->getRepository(About::class)->findAll();

        if ($about != null)
            $about = $about[0];
        else
            $about = new About();

        $form = $this->createForm(AboutFormType::class, $about);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($about);
            try {
                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'About was edited!'
                );

            } catch (\Exception $e) {
                $this->addFlash(
                    'danger',
                    'About not edited!' . $e->getMessage()
                );
            }
        }

        return $this->render('Home/about.html.twig', [
            'moderators' => $moderators,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/myFeed", name="feed")
     */
    public function showFeed(Request $request)
    {

        $page = $request->get('page');
        if ($page == null)
            $page = 1;

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $followRev = $reviewRepository->getReviewsOfFollowing($this->getUser(), $page);

        $limit = 5;
        $maxPages = ceil($followRev->count() / $limit);

        $thisPage = $page;

        return $this->render('Review/myFeed.html.twig', [
            'reviews' => $followRev,
            'maxPages' => $maxPages,
            'thisPage' => $thisPage
        ]);

    }


    /**
     * @Route("/chat", name="chat")
     */
    public function showChat(Request $request)
    {
        $id = $request->get('id');
        if ($id == null and $this->isGranted("ROLE_ADMIN")) {
            return $this->render('Errors/error404.html.twig');
        }
        $sender = $this->getUser();

        if ($id == null) {
            $receiver = $this->getDoctrine()->getRepository(User::class)->find(3);
        } else {
            if (!$this->isGranted("ROLE_ADMIN"))
                return $this->render('Errors/error403.html.twig');

            $receiver = $this->getDoctrine()->getRepository(User::class)->find($id);
            if ($receiver == null || $id == $sender->getId()) {
                return $this->render('Errors/error403.html.twig');
            }
        }

        $messages = $this->getDoctrine()->getRepository('AppBundle:Message')->getMessages($receiver, $sender);

        return $this->render('Home/chat.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @Route("/sendMessage", name="sendMessage")
     */
    public function sendMessage(Request $request)
    {

        $id = $request->get('id');

        $sender = $this->getUser();

        if ($id == null) {
            $receiver = $this->getDoctrine()->getRepository(User::class)->find(3);
        } else {
            $receiver = $this->getDoctrine()->getRepository(User::class)->find($id);
        }

        $mess = new Message();
        $mess->setContent($request->get('message'));
        $mess->setSender($sender);
        $mess->setReceiver($receiver);
        $mess->setSentAt(new \DateTime());
        $mess->setSeen(false);


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($mess);
        $entityManager->flush();

        return new Response("true");
    }

    /**
     * @Route("/readMessages", name="readMessages")
     */
    public function readMessages(Request $request)
    {

        $id = $request->get('id');

        $receiver = $this->getUser();

        if ($id == null) {
            $sender = $this->getDoctrine()->getRepository(User::class)->find(3);
        } else {
            $sender = $this->getDoctrine()->getRepository(User::class)->find($id);
        }


        $messages = $this->getDoctrine()->getRepository('AppBundle:Message')->getNewMessages($receiver, $sender);

        $entityManager = $this->getDoctrine()->getManager();

        foreach ($messages as $message) {
            $message->setSeen(true);
            $entityManager->persist($message);
            $entityManager->flush();
        }
        return new Response("true");
    }

    /**
     * @Route("/checkMessages", name="checkMessages")
     */
    public function checkNewMessages(Request $request)
    {

        $id = $request->request->get('id');

        $receiver = $this->getUser();

        if ($id == null) {
            $sender = $this->getDoctrine()->getRepository(User::class)->find(3);
        } else {
            $sender = $this->getDoctrine()->getRepository(User::class)->find($id);
        }
        $logs = array();

        $messages = $this->getDoctrine()->getRepository('AppBundle:Message')->getNewMessages($receiver, $sender);
        $i = 0;
        foreach ($messages as $message) {
            $logs[$i]['id'] = $message->getId();
            $logs[$i]['content'] = $message->getContent();
            $logs[$i]['sender'] = $message->getSender()->getUsername();
            $i++;
        }

        return new Response(json_encode($logs));

    }

}