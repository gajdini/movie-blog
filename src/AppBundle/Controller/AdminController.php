<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/13/2019
 * Time: 2:38 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Review;
use AppBundle\Entity\User;
use AppBundle\Form\RoleFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    /**
 * @Route("/admin/dashboard", name="dashboard")
 */
    public function showDashboard()
    {

        return $this->render('Admin/dashboard.html.twig');

    }

    /**
     * @Route("/admin/calendar", name="calendar")
     */
    public function showCalendar()
    {

        $reviews=$this->getDoctrine()->getRepository(Review::class)->findAll();

        $timeNow = date('Y-m-d', time());

        return $this->render('Admin/calendar.html.twig',[
            'reviews'=>$reviews,
            'today'=>$timeNow
        ]);

    }

    /**
     * @Route("/admin/users", name="users")
     */
    public function showUsers(Request $request)
    {
        $page=$request->get('page');
        if($page==null)
            $page=1;

        $limit = 50;

        $users=$this->getDoctrine()->getRepository(User::class)->getAll($page,$limit);

        $maxPages = ceil($users->count() / $limit);

        $thisPage = $page;

        return $this->render('Admin/users.html.twig',[
            'users'=>$users,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage
        ]);
    }

    /**
     * @Route("/admin/reviews", name="reviews")
     */
    public function showReviews(Request $request)
    {
        $page=$request->get('page');
        if($page==null)
            $page=1;

        $limit = 50;

        $reviews=$this->getDoctrine()->getRepository(Review::class)->getReviews($page,$limit);

        $maxPages = ceil($reviews->count() / $limit);

        $thisPage = $page;

        return $this->render('Admin/reviews.html.twig',[
            'reviews'=>$reviews,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage
        ]);
    }

    /**
     * @Route("/admin/searchResults", name="userSearch")
     */
    public function showUserSearch(Request $request)
    {
        $page=$request->get('page');
        if($page==null)
            $page=1;

        $limit = 50;

        $key=$request->get('key');

        $users=$this->getDoctrine()->getRepository(User::class)->getUsersFromSearch($key,$page,$limit);

        $maxPages = ceil($users->count() / $limit);

        $thisPage = $page;

        return $this->render('Admin/users.html.twig',[
            'users'=>$users,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage
        ]);

    }

    /**
     * @Route("/promote", name="promote")
     */
    public function promote(Request $request)
    {

        $id=$request->get('id');

        if($id==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(User::class);

        $user=$repository->find($id);

        $admin=$request->get('ad');
        $moderator=$request->get('mod');

        if($admin=="true")
            $user->addRole("ROLE_ADMIN");
        else{
            $user->setRoles(array_filter($user->getRoles(), function($val) { return $val != 'ROLE_ADMIN'; }));
        }

        if($moderator=="true")
            $user->addRole("ROLE_MODERATOR");
        else{
            $user->setRoles(array_filter($user->getRoles(), function($val) { return $val != 'ROLE_MODERATOR'; }));
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($user);

        $entityManager->flush();

        return new Response("true");
    }


    /**
     * @Route("/block", name="block")
     */
    public function blockUser(Request $request)
    {
        $id=$request->get('id');

        if($id==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(User::class);

        $user=$repository->find($id);

        $user->setBlocked(true);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($user);

        $entityManager->flush();

       return new Response("true");

    }

    /**
     * @Route("/unblock", name="unblock")
     */
    public function unblockUser(Request $request)
    {
        $id=$request->get('id');

        if($id==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(User::class);

        $user=$repository->find($id);

        $user->setBlocked(false);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($user);

        $entityManager->flush();

        return new Response("true");

    }



}