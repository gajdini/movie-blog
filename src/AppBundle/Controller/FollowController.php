<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/6/2019
 * Time: 3:19 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class FollowController extends Controller
{
    /**
     * @Route("/followModerator", name="followModerator")
     */
    public function followModerator(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $modId=$request->get('moderator');

        if($modId==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(User::class);

        $moderator=$repository->find($modId);

       $this->getUser()->setMyFollowers($moderator);

       $moderator->setFollowing($this->getUser());

       $entityManager->persist($moderator);

       $entityManager->persist($this->getUser());

        $entityManager->flush();

        return new Response("true");


    }

    /**
     * @Route("/unfollow", name="unfollow")
     */
    public function unfollow(Request $request){

        $moderatorId=$request->get('idM');

        if($moderatorId==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(User::class);

        $repository->deleteFollow($this->getUser()->getId(),$moderatorId);

        return new Response("true");

    }

}