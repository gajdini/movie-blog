<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2019
 * Time: 11:30 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Review;
use AppBundle\Entity\ReviewRepository;
use AppBundle\Entity\Subscription;
use AppBundle\Entity\User;
use AppBundle\Form\ReviewFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

class ReviewController extends Controller
{
    /**
     * @Route("/review/{rev}", name="showReview")
     */
    public function showReview($rev)
    {
        $repository = $this->getDoctrine()->getRepository(Review::class);

        $review = $repository->find($rev);

        if($review==null)
            return $this->render('Errors/error404.html.twig');

        if($review->getDeletedAt()!=null){
            $this->denyAccessUnlessGranted('ROLE_ADMIN');
        }

        $userRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:User');

        $user=$this->getUser();

        $like=null;

        if($user!=null)
            $like = $userRepository->checkIfLiked($this->getUser()->getId(),$rev);

        return $this->render('Review/review.html.twig', [
            'review' => $review,
            'like'=>$like
        ]);
    }

    /**
     * @Route("/unlike", name="unlike")
     */
    public function unlike(Request $request){

        $revId=$request->get('id');

        if($revId==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $repository->deleteLike($revId,$this->getUser()->getId());

        return new Response("true");

    }

    /**
     * @Route("/like", name="like")
     */
    public function like(Request $request){

        $entityManager = $this->getDoctrine()->getManager();

        $revId=$request->get('id');

        if($revId==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(Review::class);

        $review=$repository->find($revId);

        $this->getUser()->setLikes($review);

        $review->setLikedBy($this->getUser());

        $entityManager->persist($review);

        $entityManager->persist($this->getUser());

        $entityManager->flush();

        return new Response("true");

    }

    /**
     * @Route("/reviews/{moderator}", name="moderatorReviews")
     */
    public function showModeratorReviews(Request $request,$moderator)
    {
        $page=$request->get('page');
        if($page==null)
            $page=1;

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $reviews = $reviewRepository->getReviewsAccToModerator($moderator,$page);

        $mod=$this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy([
            'username' => $moderator
        ]);

        if($mod==null || !in_array("ROLE_MODERATOR",$mod->getRoles()))
            return $this->render('Errors/error404.html.twig');

        $follow = null;

        $sub = null;

        $user = $this->getUser();
        if ($user != null) {
            $userId = $user->getId();
            $userEmail = $user->getEmail();
            $userRepository = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:User');
            $follow = $userRepository->checkIfFollow($userId, $moderator);
            $sub = $userRepository->checkIfSubbed($userEmail, $moderator);
        }

        $limit = 5;
        $maxPages = ceil($reviews->count() / $limit);

        $thisPage = $page;

        return $this->render('Review/moderatorReviews.html.twig', [
            'reviews' => $reviews,
            'moderator' => $mod,
            'follow' => $follow,
            'sub' => $sub,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage
        ]);
    }

    /**
     * @Route("/rev/cat/{category}", name="categoryReviews")
     */
    public function showCategoryReviews(Request $request,$category)
    {
        $page=$request->get('page');
        if($page==null)
            $page=1;

        $category=urldecode($category);

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $reviews = $reviewRepository->getReviewsAccToCategory($category,$page);

        $limit = 5;
        $maxPages = ceil($reviews->count() / $limit);

        $thisPage = $page;

        return $this->render('Review/categoryReviews.html.twig', [
            'reviews' => $reviews,
            'category' => $category,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage
        ]);
    }

    /**
     * @Route("/recent", name="recentReviews")
     */
    public function showMostRecent(Request $request)
    {

        $page=$request->get('page');
        if($page==null)
            $page=1;

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $limit = 5;

        $reviews = $reviewRepository->getReviewsAccToTime($page);

        $maxPages = ceil($reviews->count() / $limit);
        $thisPage = $page;

        // Pass through the 3 above variables to calculate pages in twig
        return $this->render('Review/mostRecent.html.twig', [
            'reviews'=>$reviews,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage,
        ]);
    }

    /**
     * @Route("/popular", name="popularReviews")
     */
    public function showMostPopular(Request $request)
    {

        $page=$request->get('page');
        if($page==null)
            $page=1;

        $reviewRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Review');

        $limit = 5;

        $reviews = $reviewRepository->getReviewsAccToLikes($page);

        $maxPages = ceil($reviews->count() / $limit);
        $thisPage = $page;

        return $this->render('Review/mostPopular.html.twig', [
            'reviews'=>$reviews,
            'maxPages'=>$maxPages,
            'thisPage'=>$thisPage
        ]);
    }

    /**
     * @Route("/addReview", name="addReview")
     */
    public function addReview(Request $request,\Swift_Mailer $mailer)
    {
        if($this->getUser()->getBlocked()==0) {

            $review = new Review();

            $form = $this->createForm(ReviewFormType::class, $review);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $file = $review->getImg();
                if ($file != null) {
                    $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();
                    try {
                        $file->move(
                            $this->getParameter('brochures_directory'),
                            $fileName
                        );
                        $review->setImg($fileName);
                    } catch (FileException $e) {
                        $this->addFlash(
                            'warning',
                            'Photo was not uploaded!' . $e->getMessage()
                        );
                        $review->setImg(null);
                    }

                }

                $date = new \DateTime();

                $review->setTimePosted($date);

                $review->setModerator($this->getUser());

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($review);
                try {
                    $entityManager->flush();
                    $this->addFlash(
                        'success',
                        'Review was added!'
                    );

            $subRepository = $this->getDoctrine()->getRepository(Subscription::class);

            $emails = $subRepository->findBy(
                ['moderator' => $this->getUser()]
            );

            foreach ($emails as $email){
                $message = (new \Swift_Message('New Review'))
                    ->setFrom('aiesectirana@gmail.com')
                    ->setTo($email->getEmail())
                    ->setBody(
                        $this->renderView(
                            'Email/subscription.html.twig'
                        ),
                        'text/html'
                    );
                $mailer->send($message);

            }

                } catch (\Exception $e) {
                    $this->addFlash(
                        'danger',
                        'Review not added!' . $e->getMessage()
                    );
                }

                return $this->redirect('/reviews/' . $this->getUser()->getUsername());

            }

            return $this->render('Review/addReview.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        else{
            return $this->render('Errors/error403.html.twig');
        }
    }


    /**
     * @Route("/editReview/{rev}", name="editReview")
     */
    public function editReview(Request $request,$rev)
    {
        $review = $this->getDoctrine()
            ->getRepository(Review::class)
            ->find($rev);

        $user=$this->getUser();

        if($review!=null && $review->getModerator()==$user && $user->getBlocked()==0
        && $review->getDeletedAt()==null){
            $img=$review->getImg();

            $form = $this->createForm(ReviewFormType::class, $review);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $file = $review->getImg();
                if ($file != null) {
                    $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();
                    try {
                        $file->move(
                            $this->getParameter('brochures_directory'),
                            $fileName
                        );
                    } catch (FileException $e) {
                        $this->addFlash(
                            'warning',
                            'Photo was not uploaded!'.$e->getMessage()
                        );                    }
                    $review->setImg($fileName);
                }
                else{
                    $review->setImg($img);
                }

                $review->setModerator($user);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($review);
                try {
                    $entityManager->flush();
                    $this->addFlash(
                        'success',
                        'Review was edited!'
                    );

                } catch (\Exception $e) {
                    $this->addFlash(
                        'danger',
                        'Review not edited!'.$e->getMessage()
                    );
                }
                return $this->redirect('/review/'.$rev);
            }

            return $this->render('Review/editReview.html.twig', [
                'form' => $form->createView(),
                'review'=>$review
            ]);
        }
        else{
            return $this->render('Errors/error403.html.twig');
        }


    }

    /**
     * @Route("/deleteReview/{rev}", name="deleteReview")
     */
    public function deleteReview($rev){

        $revRepository = $this->getDoctrine()->getRepository(Review::class);

        $review = $revRepository->find($rev);

        $user=$this->getUser();

        if($review!=null && $review->getModerator()==$user && $user->getBlocked()==0
        && $review->getDeletedAt()==null){
            $entityManager = $this->getDoctrine()->getManager();

            $date = new \DateTime();

            $review->setDeletedAt($date);

            try {
                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'Review was deleted!'
                );

            } catch (\Exception $e) {
                $this->addFlash(
                    'danger',
                    'Review not deleted!'.$e->getMessage()
                );
            }

            return $this->redirect('/reviews/'.$user->getUsername());
        }
        else{
            return $this->render('Errors/error403.html.twig');
        }
    }

    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}