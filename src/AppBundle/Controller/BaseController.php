<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/11/2019
 * Time: 12:34 PM
 */

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    public function getAbout()
    {
        $aboutRepository = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:About');

        $about=$aboutRepository->getAbout();

        return new Response($about);

    }

}