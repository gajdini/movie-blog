<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/6/2019
 * Time: 4:04 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Subscription;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SubscribeController extends Controller
{
    /**
     * @Route("/unsub", name="unsub")
     */
    public function unsubscribe(Request $request){

        $id=$request->get('id');

        if($id==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(Subscription::class);

        $mod=$this->getDoctrine()->getRepository(User::class)->find($id);


        $subs=$repository->findOneBy([
            'email'=>$this->getUser()->getEmail(),
            'moderator'=>$mod
        ]);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($subs);

        $entityManager->flush();

        return new Response("true");

    }



    /**
     * @Route("/sub1", name="subscribe")
     */
    public function subToModerator(Request $request){

        $entityManager = $this->getDoctrine()->getManager();

        $modId=$request->get('moderator');

        $email=$request->get('email');

        if($modId==null || $request->isMethod('get'))
            return $this->render('Errors/error403.html.twig');

        $repository = $this->getDoctrine()->getRepository(User::class);

        $moderator=$repository->find($modId);

        $subRepository = $this->getDoctrine()->getRepository(Subscription::class);


        $subscription= new Subscription();

        $subscription->setModerator($moderator);

        if($email==null)
            $subscription->setEmail($this->getUser()->getEmail());
        else{
            $subs=$subRepository->findOneBy([
                'email'=>$email,
                'moderator'=>$modId
            ]);

            if($subs!=null) {
                return new Response("false");
            }
            else
                $subscription->setEmail($email);
        }

        $entityManager->persist($subscription);

        $entityManager->flush();

        return new Response("true");
    }

}