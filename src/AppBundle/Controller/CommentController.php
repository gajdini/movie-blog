<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2019
 * Time: 2:21 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Comment;
use AppBundle\Entity\Review;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    /**
     * @Route("/addComment", name="addComment")
     */
    public function addComment(Request $request){

        $reviewId = $request->get('review');

        if($reviewId==null || $request->isMethod('get')){
            return $this->render('Errors/error403.html.twig');
        }
        else {
            $entityManager = $this->getDoctrine()->getManager();

            $content = $request->get('content');

            $repository = $this->getDoctrine()->getRepository(Review::class);

            $review = $repository->find($reviewId);

            $date = new \DateTime();

            $comment = new Comment();
            $comment->setContent($content);
            $comment->setTimeDone($date);
            $comment->setUser($this->getUser());
            $comment->setReview($review);

            $entityManager->persist($comment);

            $entityManager->flush();

            return new Response("true");
        }

    }

}