<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/4/2019
 * Time: 10:04 AM
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Review;
use AppBundle\Entity\Category;
use AppBundle\Entity\Subscription;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $categories=['Thriller','Comedy','Documentary','Romance','Sci-fi','Horror','Psychological','Drama'];
        $user = $manager->getRepository('AppBundle:User')->find(2);
        $timeNow=date('Y-m-d h:i:s', time());
        $date = new \DateTime($timeNow);


        for ($i = 0; $i < 20; $i++) {

            $review=new Review();
            $review->setTitle('Title'.$i);
            $review->setText($i.'. This blog post shows a few different types of content that\'s supported and styled with Bootstrap. Basic typography, images, and code are all supported.');
            $review->setModerator($user);
            $review->setTimePosted($date);
            $manager->persist($review);

            $subscription=new Subscription();
            $subscription->setEmail('user'.$i.'@gmail.com');
            $subscription->setModerator($user);
            $manager->persist($subscription);

        }
        for ($i = 0; $i < 8; $i++) {
            $category=new Category();
            $category->setName($categories[$i]);
            $manager->persist($category);
        }

        $manager->flush();
    }


}