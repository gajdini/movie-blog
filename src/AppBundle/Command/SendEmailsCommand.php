<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/19/2018
 * Time: 4:37 PM
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class SendEmailsCommand
 * @package AppBundle\Command
 * @author Greisa
 */
class SendEmailsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:send-emails')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sends queued emails.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you send emails...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processString = sprintf(
            'php %s/../bin/console swiftmailer:spool:send',
            $this->getContainer()->get('kernel')->getRootDir()
        );


        $process = new Process($processString);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();
    }
}