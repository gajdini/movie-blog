<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/1/2019
 * Time: 3:46 PM
 */

namespace AppBundle\EventListener;


use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class RedirectAfterRegistrationSubscriber implements EventSubscriberInterface
{

    use TargetPathTrait;

    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router=$router;
    }

    public function onRegistrationSuccess(FormEvent $event){

//        $user = $event->getForm()->getData();
//        $user->setEnabled(false);
//        if (null === $user->getConfirmationToken()) {
//            $user->setConfirmationToken($this->tokenGenerator->generateToken());
//        }
//        $this->mailer->sendConfirmationEmailMessage($user);
//        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());
//        $url = $this->router->generate('fos_user_registration_check_email');
//        $event->setResponse(new RedirectResponse($url));

        $url=$this->getTargetPath($event->getRequest()->getSession(),'main');

        if(!$url){
            $url = $this->router->generate('home');
        }

        $response = new RedirectResponse($url);
        $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess'
        ];
    }

}