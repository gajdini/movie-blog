<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/11/2019
 * Time: 12:42 PM
 */

namespace AppBundle\Twig;


use AppBundle\Entity\Message;
use AppBundle\Entity\MessageRepository;
use AppBundle\Entity\User;
use Twig\Extension\AbstractExtension;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    protected $doctrine;

    // Retrieve doctrine from the constructor

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('about', [$this, 'getAbout']),
            new TwigFunction('archives', [$this, 'getArchives']),
            new TwigFunction('addedReviewsAccToWeek', [$this, 'addedReviewsAccToWeek']),
            new TwigFunction('addedCommentsAccToWeek', [$this, 'addedCommentsAccToWeek']),
            new TwigFunction('getNrMessages', [$this, 'getNrMessages']),

        ];
    }

    public function getArchives()
    {
        $reviewRepository = $this->doctrine
            ->getManager()
            ->getRepository('AppBundle:Review');

        $archives=$reviewRepository->getMonths();

        return $archives;

    }

    public function addedReviewsAccToWeek(){
        $timeNow=date('Y-m-d h:i:s', time());
        $t1= date('Y-m-d h:i:s', strtotime('-7 days', strtotime($timeNow)));

        $sql="SELECT dayname(review.time_posted) as d, count(review.id) as cnt
              from review
              where date(time_posted) < :tnow
              and date(time_posted)>= :t1
              group by d";

        $conn=$this->doctrine->getEntityManager()->getConnection();
        $smtm=$conn->prepare($sql);
        $smtm->bindParam(':tnow', $timeNow);
        $smtm->bindParam(':t1', $t1);
        $smtm->execute();
        return $smtm->fetchAll();
    }

    public function addedCommentsAccToWeek(){
        $timeNow=date('Y-m-d', time());
        $t1= date('Y-m-d', strtotime('-7 days', strtotime($timeNow)));

        $sql="SELECT dayname(comment.time_done) as d, count(comment.id) as cnt
              from comment
              where date(time_done) < :tnow
              and date(time_done)>= :t1
              group by d";

        $conn=$this->doctrine->getEntityManager()->getConnection();
        $smtm=$conn->prepare($sql);
        $smtm->bindParam(':tnow', $timeNow);
        $smtm->bindParam(':t1', $t1);
        $smtm->execute();
        return $smtm->fetchAll();
    }


    public function getAbout()
    {
        $aboutRepository = $this->doctrine->getManager()
            ->getRepository('AppBundle:About');

        $about = $aboutRepository->getAbout();

        return $about;
    }

    public function getNrMessages($u){

        $sender = $this->doctrine->getManager()->getRepository(User::class)->find(3);

        $rep = $this->doctrine->getManager()
            ->getRepository('AppBundle:Message');

        $nr=$rep->getNewMessages($u,$sender);

        return count($nr);

    }

    public function getName()
    {
        return 'Twig myCustomName Extensions';
    }

}