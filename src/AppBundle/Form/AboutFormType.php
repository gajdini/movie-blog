<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/8/2019
 * Time: 10:14 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\About;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AboutFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('about', TextareaType::class, array(
            'label' => 'Name',
            'attr' => array('class' => 'form-control'),
        ))
            ->add('save', SubmitType::class, array(
                'label' => 'Edit',
                'attr' => array('class' => 'btn btn-primary'),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => About::class,
        ]);
    }
}