<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/8/2019
 * Time: 10:14 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\Review;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ReviewFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array(
            'label' => 'Title',
            'required'=>false,
            'attr' => array('class' => 'form-control'),
        ))
            ->add('link', TextareaType::class, array(
                'label' => 'Youtube Link',
                'required' => false,
                'attr' => array('class' => 'form-control'),
            ))
            ->add('img', FileType::class, array(
                'label' => 'Cover Image',
                'required' => false,
                'data_class' => null,

            ))
            ->add('categories', EntityType::class, [
                'label'     => 'Categories',
                'multiple'  => true,
                'choices_as_values' => true,
                'class'=>'AppBundle\Entity\Category',
                'choice_label'=>'name',
                'required'=>false,
            ])
            ->add('text', CKEditorType::class, array(
                'label' => 'Content',
                'attr' => array('class' => 'form-control'),
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Create Review',
                'attr' => array('class' => 'btn btn-primary'),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Review::class,
        ]);
    }
}