<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/8/2019
 * Time: 10:14 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, array(
            'label' => 'Username',
            'attr' => array('class' => 'form-control'),
        ))
            ->add('firstName', TextType::class, array(
            'label' => 'First Name',
            'attr' => array('class' => 'form-control'),
        ))
            ->add('lastName', TextType::class, array(
                'label' => 'Last Name',
                'attr' => array('class' => 'form-control'),
            ))
            ->add('bio', TextareaType::class, array(
                'label' => 'Bio',
                'required' => false,
                'attr' => array('class' => 'form-control'),
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Edit',
                'attr' => array('class' => 'btn btn-primary'),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}