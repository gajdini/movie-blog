-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 03:30 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movieblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`about`) VALUES
('Just a simple blog for movie lovers!');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(7, 'Action'),
(17, 'Animation'),
(14, 'Biopic'),
(18, 'Books/Comics'),
(2, 'Comedy'),
(5, 'Documentary'),
(6, 'Drama'),
(16, 'Fantasy'),
(1, 'Horror'),
(8, 'Remake'),
(12, 'Sci-fi'),
(4, 'Thriller'),
(15, 'Trailers'),
(13, 'TV');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `review_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `time_done` datetime NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `review_id`, `user_id`, `time_done`, `content`) VALUES
(1, 2, 5, '2019-02-19 16:21:41', 'Great review! Can\'t wait to watch it!'),
(2, 6, 8, '2019-02-19 16:48:40', 'Nice review! I too like this movie.'),
(3, 3, 8, '2019-02-19 17:21:50', 'Quite scary!'),
(4, 2, 9, '2019-02-24 17:58:26', 'Good one!'),
(5, 6, 4, '2019-02-25 09:45:48', 'Great!!!!');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `moderator_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`moderator_id`, `follower_id`) VALUES
(4, 6),
(4, 8),
(9, 4);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `review_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`review_id`, `user_id`) VALUES
(2, 5),
(2, 8),
(2, 9),
(3, 8),
(5, 8),
(6, 8),
(11, 4);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `sent_at` datetime NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `sender_id`, `receiver_id`, `sent_at`, `content`, `seen`) VALUES
(1, 5, 3, '2019-02-26 16:14:29', 'hey\n', 1),
(2, 5, 3, '2019-02-26 16:14:38', 'help please\n', 1),
(3, 3, 5, '2019-02-26 16:14:48', 'with what\n', 1),
(4, 3, 5, '2019-02-26 16:16:54', 'yes please\n', 1),
(5, 4, 3, '2019-02-26 21:46:50', 'Can you help me with something?\n', 1),
(6, 3, 6, '2019-02-26 23:19:41', 'I have blocked you!\n', 0),
(7, 9, 3, '2019-02-27 11:28:36', 'Hello\n', 1),
(8, 3, 9, '2019-02-27 11:28:44', 'Hey\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190219131108', '2019-02-19 13:11:42'),
('20190219135837', '2019-02-19 13:59:08'),
('20190219141240', '2019-02-19 14:13:31'),
('20190221135053', '2019-02-21 13:51:11'),
('20190225161816', '2019-02-25 16:19:09'),
('20190226142218', '2019-02-26 14:22:38'),
('20190226143132', '2019-02-26 14:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `moderator_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `moderator_id`, `title`, `text`, `link`, `deleted_at`, `img`, `time_posted`) VALUES
(2, 4, 'Long Shot Trailer Starring Seth Rogen and Charlize Theron', '<p>After taking a couple of years away from any major starring roles, Seth Rogen is back with a new comedy this summer that finds him reteaming with&nbsp;<em>50/50</em>&nbsp;and&nbsp;<em>The Night Before</em>&nbsp;director Jonathan Levine. This one harkens back to the movie that made him a huge star,&nbsp;<em>Knocked Up</em>, in that it finds Rogen playing an unattractive schlub named Fred Flarsky who enters into an unlikely relationship with an otherwise unattainable woman. Charlize Theron plays the love interest, who in this case also happens to be the U.S. Secretary of State and his former babysitter, but she hires Flarsky as a campaign writer when she decides to run for president. Yeah, it feels like even more of a stretch than usual and also a thinly-veiled excuse to work in some political humour, but if anyone can still make this work it&rsquo;s probably Charlize Theron.&nbsp;<em>Long Shot</em>&nbsp;co-stars June Diane Raphael, Alexander Skarsgard, O&rsquo;Shea Jackson Jr., Andy and Randall Park and it hits theatres on May 3rd. Check out the trailer below and see what you think.</p>', 'https://www.youtube.com/watch?v=KZL3EikI9tY', NULL, '9c77d39bbe3b28bab93710a70698e297.jpeg', '2019-02-19 15:17:29'),
(3, 4, 'Hereditary Review', '<p><em>Hereditary</em>&nbsp;is, in fact, a horror movie. That alone shouldn&rsquo;t be considered a spoiler since A24&rsquo;s marketing material has been strongly trying to bring that point home to us, the ticket-buying audience, since the get-go. However, any astute moviegoer who has come across any of the myriad pieces lumping it together in the same breath with&nbsp;<em>The VVitch</em>&nbsp;and&nbsp;<em>The Babadook</em>&nbsp;might have approached the box office, if at all, with trepidation. And with just reason.</p>\r\n\r\n<p>Like Robert Eggers&rsquo;&nbsp;<em>The VVitch</em>&nbsp;(which is, indeed, a horror offering) and Jennifer Kent&rsquo;s&nbsp;<em>The Babadook</em>&nbsp;(which isn&rsquo;t), Ari Aster&rsquo;s feature writing-directing debut, is simply put, not a crowd pleaser. Which does not necessarily label it at once as pretentious (which, in my humble opinion, none of those three movies are) but it does address the fact that it uses genre with different purposes in mind.</p>\r\n\r\n<p>Saying that the household formed by Toni Colette, Gabriel Byrne and their kids Alex Wolff and Milly Shapiro is dysfunctional is the greatest understatement in history. Grandma was a manipulative matriarch (which ultimately will end up being her most benign treat), Colette&rsquo;s brother starved himself to death while in the throes of depression, she herself suffers from sleepwalking episodes that lead her to the brink of unthinkable actions, and daughter Shapiro ambles around in an aloof cloud, seeing and experiencing a world all of her own. Indulging in specifics of plot would not really help elucidate its potential for success or failure and would ultimately come off as a disservice.</p>\r\n\r\n<p><img alt=\"\" src=\"http://filmjunk.com/images/weblog/2018/06/hereditary1.jpg\" style=\"height:348px; width:500px\" /></p>\r\n\r\n<p>Nevertheless, the first hour and forty minutes of the movie (of its two hour and bit total running time) do not even try to be scary. That is a movie by itself, one dealing with grief, loss and mourning. And it does so extremely well. There is death all around: the movie starts with a card taking up the middle of the screen showing us grandma&rsquo;s obituary and then there is the end of life or the threat of it coming from old age, murder, suicide, accident, omnipresent. During this stretch the only present fear comes from having to become that person, the one who lost someone, the one who is about to leave, the one who wants to leave. These fears are very much real and soul-stopping and Aster is able to orchestrate a permanent mood of mostly mute despair and unrelenting dread with his depiction of lives lit by mellow tones that turn the surrounding shadows with an even colder edge. These tones may be, at turns, both embracing and heartbreakingly melancholic.</p>', NULL, NULL, NULL, '2019-01-19 15:20:20'),
(4, 4, 'Deadpool 2 Review', '<p>Directed by: David Leitch<br />\r\nWritten by: Rhett Reese, Paul Wernick and Ryan Reynolds (screenplay), Rob Liefeld and Fabian Nicieza (comics)<br />\r\nStarring: Ryan Reynolds, Josh Brolin, Zazie Beetz, Morena Baccarin, Brianna Hildebrand, T.J. Miller, Julian Dennison, Karan Soni</p>\r\n\r\n<p><img alt=\"\" src=\"http://filmjunk.com/images/weblog/2018/05/deadpool2_2.jpg\" style=\"height:357px; width:500px\" /></p>\r\n\r\n<p>One of the most commonly heard complaints regarding the original&nbsp;<em>Deadpool</em>&nbsp;is that, like&nbsp;<em>Kick-Ass</em>&nbsp;six years prior, it promised to deliver a different kind of superhero movie but, by the end, it was simply another one in an ever-growing bunch.&nbsp;<em>Kick-Ass</em>, in fact, was something of a subpar entry in the subgenre even before our ordinary Joe protagonist was comfortably manning a jetpack and shooting missiles at bad guys. But the fact is&nbsp;<em>Deadpool</em>&nbsp;was never born out of a desire to deconstruct anything; it promised to offer simply another superhero movie and yet by the end it had delivered something so undeniably and satisfyingly different that it became the pop culture milestone it is now, like it or not.</p>\r\n\r\n<p>When it opened on Valentine&rsquo;s Day weekend, two years ago, it was obviously aiming at capitalizing on counterprogramming but what the vast majority of onlookers never counted on was that it would, in fact, legitimately work as a date movie on its own right. There is a real love story breathing in there and whether it works or not is irrelevant; the filmmakers cared and put every effort in making sure that it would. And they succeeded: Morena Baccarin, although saddled with the de rigueur revealing outfits and the titillating decision to make her a stripper excels at selling us her character as way more than just flesh. When things start going sour for the couple it is a lot easier to actually feel invested. Chief among the many reasons for the original&nbsp;<em>Deadpool</em>&lsquo;s unexpected success was the fact that, hidden but latent in the midst of its grotesquely violent profanity, there was genuine heart.</p>\r\n\r\n<p>Alone as the single reason for the sequel&rsquo;s disappointing unveiling is the fact that there is genuine heart to be found all over it, hidden no more, permeating most of it most of the time. The filmmakers ask us, not unreasonably, to care about these characters and I still do. However, the intimacy of these people&rsquo;s love (sentimental if not sexual), previously delicately depicted, is now hammered home as bluntly as possible. Their ultimately inevitable emotional payoff is now not merely forcing itself on us but actively trying to rape its way into our hearts. Big mistake.</p>\r\n\r\n<p>A lot has been said about how original director Tim Miller&rsquo;s departure meant the loss of the real driving creative force behind the project but that&rsquo;s hardly the case. Replacement helmer David Leitch, coming off of uncredited work in&nbsp;<em>John Wick</em>&nbsp;and&nbsp;<em>Atomic Blonde</em>, his first official directorial credit, makes sure the action unfolds appropriately kinetically and clearly shot. He also expands from the scant two locations seen in the first film and skillfully manages the twofold increased budget, rightfully poising himself as an A-list player following this and the upcoming&nbsp;<em>Fast &amp; Furious</em>&nbsp;spin-off.</p>\r\n\r\n<p><img alt=\"\" src=\"http://filmjunk.com/images/weblog/2018/05/deadpool2_3.jpg\" style=\"height:336px; width:500px\" /></p>\r\n\r\n<p>No, the stone cold core of this series was and still is the writing by the duo of Rhett Reese and Paul Wernick, now joined by producer and star Reynolds. The expected fourth-wall breaking and pop culture references abound and their apparently augmented amount (I didn&rsquo;t count but they came at a breakneck pace that would make Abrahams and the Zuckers proud) vainly tries to make up for their also increased hit-or-miss nature. It is in the screenplay where the fault lies. Following an unexpectedly somber beginning that doesn&rsquo;t resemble the perenially ludic tone established a couple years ago, the movie at last picks up when Josh Brolin&rsquo;s Cable shows up.</p>\r\n\r\n<p>From then on we are treated to the sequel that most of the moviegoing audiences were already entertaining in their heads in some way or other: extreme violence (lots of it Looney Toones-inspired), endless ADR comedic enhancement, cameos (both from fictional and real life people), and gags upon gags. That is until the end when, once again, the screenplay asks me to hit the brakes and take the climax seriously, in the most blatantly shameless &rsquo;90s sitcom manner. Saying that such attempt fails is a gigantic understatement, particularly when it comes to what is, presumably, its most heart-wrenching moment, a disastrously timed scene that goes on and on endlessly, failing spectacularly at being either honestly affecting or truly funny. The most flummoxing aspect of it all is that the peak of all of this unexpected misery becomes null once the end credits start rolling (if you stick around that long).</p>\r\n\r\n<p>Reynolds, however, remains great at the schtick he&rsquo;s been perfecting ever since his&nbsp;<em>Van Wilder</em>&nbsp;days with the usual blend of leading Alpha male and teenage low-brow humor if you like that kind of thing (I do) and Brolin infuses some (probably as unnecessary as welcome) pathos to Rob Liefeld&rsquo;s feetless and pouch-plagued fever dream. Zazie Beetz as Domino, the most important addition to the universe, gets the biggest share of lines granted to the newcomers and makes the best of them but Terry Crews&rsquo; contribution is barely more than a glorified walk-on. The rest of the supporting cast is just as good and therefore heartbreaking in the scarcity of their screen time.</p>\r\n\r\n<p>This is a disappointing&nbsp;<em>Deadpool</em>&nbsp;sequel which, in the end, still is a way better and more satisfying movie (comic book-based or otherwise) than most average summer blockbuster offerings. However, it is pretty depressing having to admit that the filmmakers forgot what they themselves had already so boldly understood. That audiences are, in fact, able to infer, understand and most importantly feel without anyone having to force us to.&nbsp;</p>', NULL, NULL, NULL, '2019-02-09 15:46:49'),
(5, 4, 'The Avengers: Infinity War Review', '<p>The Avengers: Infinity War<br />\r\nDirected by: Anthony and Joe Russo<br />\r\nWritten by: Christopher Markus and Stephen McFeely<br />\r\nStarring: Robert Downey Jr., Chris Hemsworth, Mark Ruffalo, Chris Evans, Scarlett Johansson, Benedict Cumberbatch, Don Cheadle, Tom Holland, Chadwick Boseman, Paul Bettany, Elizabeth Olsen, Anthony Mackie, Sebastian Stan, Danai Gurira, Letitia Wright, Dave Bautista, Zoe Saldana, Josh Brolin, Chris Pratt, Bradley Cooper, Gwyneth Paltrow, Tom Hiddleston, Peter Dinklage</p>\r\n\r\n<p><img alt=\"\" src=\"http://filmjunk.com/images/weblog/2018/04/avengersinfinitywar1.jpg\" style=\"height:333px; width:500px\" /></p>\r\n\r\n<p>I&rsquo;ll keep this brief for all of you who don&rsquo;t really care all that much &ndash; or at all &ndash; for what is known as the Marvel Cinematic Universe: don&rsquo;t bother.&nbsp;<em>The Avengers: Infinity War</em>&nbsp;offers the level of quality (cookie-cutter, as deemed by most of you) you have come to expect from this franchise and neither improves it nor drops the ball. For all of you who have not bothered to follow any of its previous installments, the warning comes emphasized: you&rsquo;ll be watching two hours and twenty minutes of mostly CGI action that frames a thoroughly nonsensical plot filled to the brim with an inordinate amount of &ldquo;characters&rdquo; who are really not introduced or explained in any way whatsoever and who, hence, will mean nothing to you.</p>\r\n\r\n<p>For all of you this review is over. You can stop reading. Until next time.</p>\r\n\r\n<p>If you care, however&hellip; For you, who have witnessed this ten-year journey with more benign eyes, I recap: back in 2008, two months and a half before Christopher Nolan&rsquo;s insanely anticipated&nbsp;<em>The Dark Knight</em>&nbsp;opened to droolingly orgasmic reviews, an overwhelmingly vast majority of cinephiles mocked the nascent Marvel Studios&rsquo; naive decision to offer the big budget treatment to&nbsp;<em>Iron Man</em>, a C-list character if there ever was one. It was, of course, bound to be an utter disaster. Four years later, almost to the date, people vaticinated the oncoming embarrassment that&nbsp;<em>The Avengers</em>&nbsp;was about to become, what with its overabundance of characters and unavoidable absurdity. In 2014, the worst idea yet:&nbsp;<em>The Guardians of the Galaxy</em>. Not even F-list worthy, virtually unheard of, with a talking raccoon in their midst.</p>\r\n\r\n<p><em>Ant-Man</em>&nbsp;(seriously?),&nbsp;<em>Doctor Strange</em>&nbsp;(bottom of the barrel not scraped yet?), and&nbsp;<em>Black Panther</em>followed, all of them paving the way for the most impossible idea of them all: a pair of movies that would bring over 20 main characters together to fight a common threat. The trailers confirmed it: there were a lot of superheroes doing their thing, even more than the original Avengers or the Schumacher Batman sequels themselves ever dared to imagine. There was Spider-Man with a new, more tech advanced look that would no doubt sport nipples and a pocket for his American Express Spidey Card. There was the somber score foreshadowing a self-serious tone to be undoubtedly matched by an overindulgent running time. It was bound to be a mess, this time for real, the first Marvel Studios misstep. It had to be.</p>\r\n\r\n<p>And, I confess, I walked into the theater ready for anything. Anything but what I ultimately got.&nbsp;<em>The Avengers: Infinity War</em>&nbsp;is a good movie, a great movie actually, but more than that, it is a miracle. Hollywood doesn&rsquo;t have any right to come up with such amazingly flawless exercises in entertainment but they have been doing it, time and again, since Marvel Studios stepped up to show the world just exactly how.</p>\r\n\r\n<p>It has an abundance but not an overabundance of characters since, incredibly, Christopher Markus and Stephen McFeely (and probably some additional uncredited help) pull off the pacing, the rhythm and the balancing act as a whole, making the entire proceedings feel as easily achievable as breathing. This is not just good comic book superhero screenwriting, this is excellent writing, period. When the promise of worlds colliding &ndash; both literally and figuratively &ndash; finally pays off it fully delivers, packing a wallop that, although initially played for laughs, builds up towards unavoidable dread, reactions expertly ellicited by an entire cast beyond reproach.</p>\r\n\r\n<p>You&rsquo;ve got Pratt and Downey Jr. engaging in an endless ego match, Holland perenially awestruck, Hemsworth unaffected by maintaining a conversation with a &ldquo;rabbit&rdquo;, Hiddleston drawing our attention one way and the other with his customary sleight of mouth, and Evans reaffirming his status as the world&rsquo;s beacon of hope. But all of them are unexpectedly led by what may just be the most relevant and affecting motion capture performance ever by Josh Brolin as Thanos, a genocidal bully with the aspirations of a philosopher. The digital artistry on display goes beyond the usual standards but Brolin&rsquo;s voice alone makes the film&rsquo;s main nemesis disturbingly real and, even though he doesn&rsquo;t force us to empathize with him, he is impossible not to fear.</p>', NULL, NULL, NULL, '2019-01-09 16:07:28'),
(6, 4, 'The Neon Demon Review', '<p>The Neon Demon<br />\r\nWritten and Directed by: Nicolas Winding Refn<br />\r\nStarring: Elle Fanning, Karl Glusman, Jena Malone, Bella Heathcote, Abbey Lee, Desmond Harrington, Christina Hendricks, Keanu Reeves</p>\r\n\r\n<p><img alt=\"\" src=\"http://filmjunk.com/images/weblog/2016/06/neondemon2.jpg\" style=\"height:334px; width:500px\" /></p>\r\n\r\n<p>After riding the critical wave with&nbsp;<em>Drive</em>&nbsp;and then crashing down with&nbsp;<em>Only God Forgives</em>, Nicholas Winding Refn has returned with the erotically stylized&nbsp;<em>The Neon Demon</em>. What&rsquo;s even stranger than the content in the film is the fact that this is playing in multiplexes, as it has become apparent Refn is not interested in making another&nbsp;<em>Drive</em>. That was the closest example of commercial filmmaking from Refn, whose art house sensibilities are in full force with&nbsp;<em>The Neon Demon</em>. Branded by many as style over substance, this criticism may or may not be warranted as style is the forefront of Refn&rsquo;s intentions. The narrative is presented through purely visual lens with this viscerally kinky journey through Los Angeles&rsquo; fashion scene.</p>\r\n\r\n<p>We are introduced to Jesse (Elle Fanning) who moves to L.A. aspiring to make a career out of modeling. Jesse carries an aura of innocence to her, without realization of how virulent the world she&rsquo;s venturing into is. The supporting cast includes Jena Malone as makeup artist Ruby, Bella Heathcote and Abbey Lee as two models who instantly feel threatened by Jesse&rsquo;s arrival, and very small appearances by Keanu Reeves and Christina Hendricks.</p>\r\n\r\n<p>Jesse impresses everyone she encounters and steals the spotlight from her competition. One of the models shares all of the plastic surgery she went through to meet the unbelievable standards of the industry. Jesse&rsquo;s beauty is entirely natural, making the girls around her even more envious.</p>\r\n\r\n<p>Jesse tells her doting boyfriend that she can&rsquo;t sing or dance or write, but she&rsquo;s pretty. His character attempts to be the moral voice in the film, but it&rsquo;s unrealized. Since these two are the closest things to real people in the movie, their dull dialogue makes their romance feel unearned. It also let&rsquo;s Jesse&rsquo;s turn from nice girl to one of the gorgeous zombies happen quicker.</p>\r\n\r\n<p>Jena Malone may be the stand out of the film. Aside from a particular scene that maybe shouldn&rsquo;t be talked about, her character has the most to work with. The other characters are supposed to look pretty and lifeless, but her role offers range and moves into some very dark places as the film progresses.</p>', NULL, NULL, 'bf8d32020fa00319df99a0d359d2b2d4.jpeg', '2019-02-10 16:27:50'),
(7, 4, 'The Dirt Trailer: The Infamous Motley Crue Memoir Heads to Netflix', '<p>After over a decade spent in development hell, the movie adaptation of Motley Crue&rsquo;s autobiography&nbsp;<em>The Dirt</em>&nbsp;is finally about to be unleashed on the world. The project started life at MTV before moving to Focus Features and finally ending up at Netflix, which seems like the appropriate home for such a notorious tale. The movie is directed by Jeff Tremaine (<em>Jackass</em>) and stars Douglas Booth, Iwan Rheon, Machine Gun Kelly and Daniel Webber as Nikki Sixx, Mick Mars, Tommy Lee and Vince Neil respectively (Liam Hemsworth and Emory Cohen were previously rumoured for lead roles but never ended up signing on). Will this be the kind of no holds barred musical biopic that&nbsp;<em>Bohemian Rhapsody</em>should have been?&nbsp;<em>The Dirt</em>&nbsp;arrives globally on Netflix on March 22nd; check out the trailer after the jump and let us know what you think.</p>\r\n\r\n<p>&nbsp;</p>', 'https://www.youtube.com/watch?v=-NOp5ROn1HE', NULL, '6428185fc55fa6c26dfd44408cf1bb3f.jpeg', '2019-02-20 10:01:33'),
(8, 4, 'The Big Lebowski and Carrie Bradshaw Return in Super Bowl Commercial', '<p>Fans of The Coen Brothers&rsquo;&nbsp;<em>The Big Lebowski</em>&nbsp;took notice last week when Jeff Bridges put up a&nbsp;<a href=\"https://www.instagram.com/p/BtBmdleAk3M/?hl=en\" target=\"_blank\">mysterious post</a>&nbsp;on Instagram that seemed to hint that he would be reprising his role as The Dude in the near future. The post teased a launch date of Feb. 3rd, 2019, which also happens to be Super Bowl Sunday, thus leading many people to conclude that he would be appearing in a Super Bowl commercial. Now the truth has been revealed and yes, that is indeed accurate: it was all part of a marketing campaign for Stella Artois. The ad also sees Sarah Jessica Parker returning as her&nbsp;<em>Sex and the City</em>&nbsp;character Carrie Bradshaw as well. So while this may be disappointing, it should not come as a surprise&hellip; The Coens have stated multiple times before that there will never be a&nbsp;<em>Big Lebowski</em>&nbsp;sequel. It is unclear if a different version will air on Sunday but in the meantime you can check out the commercial after the jump and see what you think.</p>', NULL, NULL, 'e38dccbe6179d66330fe5a4b547ed728.jpeg', '2019-02-20 10:28:42'),
(9, 4, 'Sofia Coppola to Reteam with Bill Murray for On the Rocks', '<p>While everyone continues to wonder and speculate about whether Bill Murray could be convinced to return for another&nbsp;<em>Ghostbusters</em>&nbsp;movie, we can confirm in the meantime that he will be reteaming another well-known collaborator in the near future. Murray has reportedly signed on to star in Sofia Coppola&rsquo;s next film&nbsp;<em>On the Rocks</em>&nbsp;alongside Rashida Jones. This is the first project in the&nbsp;<a href=\"https://variety.com/2018/film/news/apple-a24-films-1203029800/\" target=\"_blank\">recently announced partnership</a>&nbsp;between Apple and A24 Films and Murray&rsquo;s third project with Coppola. Hit the jump for more info.</p>\r\n\r\n<p>According to&nbsp;<a href=\"https://deadline.com/2019/01/apple-a24-partner-on-first-film-sofia-coppola-bill-murray-reunion-on-the-rocks-1202535587/\">Deadline</a>, Bill Murray and Rashida Jones will star in Sofia Coppola&rsquo;s upcoming film&nbsp;<em>On the Rocks</em>&nbsp;for Apple and A24. The story centers on &ldquo;a young mother who reconnects with her larger than life playboy father on an adventure through New York.&rdquo; Clearly it contains elements of both&nbsp;<em>Lost in Translation</em>&nbsp;and&nbsp;<em>Somewhere</em>&nbsp;and it sounds like a potential return to form for Coppola after her last film,&nbsp;<em>The Beguiled</em>, which was a bit of a departure.</p>\r\n\r\n<p>Coppola last teamed with Murray on&nbsp;<em>A Very Murray Christmas</em>&nbsp;for Netflix and she previously worked with A24 on&nbsp;<em>The Bling Ring</em>. At the moment it is unclear if&nbsp;<em>On the Rocks</em>&nbsp;will get a theatrical release or if it will go directly to Apple&rsquo;s upcoming streaming service, but given the reputation of&nbsp;<em>Lost in Translation</em>, there is a chance they could be looking at this as a possible awards contender for next year. Production is expected to start this spring. Are you looking forward to&nbsp;<em>On the Rocks</em>?</p>', NULL, NULL, NULL, '2019-02-20 10:30:01'),
(10, 4, 'Weird City Trailer: Jordan Peele’s Other Upcoming Sci-Fi Series', '<p>Jordan Peele is a pretty busy man these days with multiple projects in development for both the big screen and small screen. Among his upcoming TV projects is a reboot of&nbsp;<em>The Twilight Zone</em>, which is expected to debut later this year on CBS All Access, but before that hits he has another sci-fi anthology series coming to YouTube called&nbsp;<em>Weird City</em>. This one was co-created with&nbsp;<em>Key &amp; Peele</em>collaborator Charlie Sanders and it seems to essentially be a comedic version of&nbsp;<em>Black Mirror</em>, taking place in not too distant future metropolis called Weird. It has an impressive cast that includes Rosario Dawson, Michael Cera, Awkwafina, Sara Gilbert, Dylan O&rsquo;Brien, Ed O&rsquo;Neill, Mark Hamill, Yvette Nicole Brown, Gillian Jacobs and LeVar Burton, but it does look pretty cartoony. The six-episode first season of&nbsp;<em>Weird City</em>&nbsp;arrives on YouTube Premium on Feb. 13th; check out the trailer below and see what you think.</p>', 'https://www.youtube.com/watch?v=raJJbbiKtlY', NULL, '5202305ae3f1592ffea50f2cce0a14ee.jpeg', '2019-02-20 10:31:35'),
(11, 9, 'Dragged Across Concrete Trailer Starring Mel Gibson and Vince Vaughn', '<p>With&nbsp;<em>Bone Tomahawk</em>&nbsp;and&nbsp;<em>Brawl in Cell Block 99</em>, S. Craig Zahler carved out a reputation as a writer and director of brutal, uncompromising and deliberately paced genre films. His output may not be for everyone, but there is definitely a subset of cinephiles eagerly anticipating his next film,&nbsp;<em>Dragged Across Concrete</em>. The movie finds him reteaming with Vince Vaughn and Don Johnson while also adding Mel Gibson to the mix; Vaughn and Gibson play dirty cops who are suspended from the force and turn to the criminal underworld for justice. A trailer has arrived this week and although it is pretty low key, reviews out of Venice last year suggest that this still fits into Zahler&rsquo;s body of work nicely.&nbsp;<em>Dragged Across Concrete</em>&nbsp;co-stars Jennifer Carpenter, Michael Jai White, Tory Kittles, Laurie Holden, Thomas Kretschmann and Udo Kier and it hits select theatres and VOD on March 22. Check out the trailer after the jump and see what you think.</p>', NULL, NULL, '5bbd682fbe9a7a9340958f7634cd2fbe.jpeg', '2019-02-22 11:38:21'),
(12, 9, 'The Man Who Killed Don Quixote Gets a U.S. Trailer and Release Date', '<p>As promised back in December, Terry Gilliam&rsquo;s ill-fated passion project&nbsp;<em>The Man Who Killed Don Quixote</em>&nbsp;is miraculously coming to theatres this spring. At the time, it was hinted that it would be a limited engagement through Fathom Events but now we have confirmation that it will be one night only. So mark Wednesday, April 10th on your calendars because that is the only chance you will have to see it in the big screen.&nbsp;<em>The Man Who Killed Don Quixote</em>&nbsp;will screen at over 700 theatres in the U.S. and &ldquo;select&rdquo; Canadian locations with exclusive bonus features (perhaps a taste of the&nbsp;<em>Lost in La Mancha</em>&nbsp;documentary follow-up,&nbsp;<em>He Dreams of Giants</em>?). You can buy tickets online&nbsp;<a href=\"https://www.fathomevents.com/events/the-man-who-killed-don-quixote/theaters\" target=\"_blank\">here</a>, but in the meantime, check out the brand new U.S. trailer after the jump and let us know what you think.</p>', 'https://www.youtube.com/watch?v=29S2P5ntGOY', NULL, 'aa6c52abd43436ecbe020a6a64d7b05c.jpeg', '2019-02-27 15:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `review_category`
--

CREATE TABLE `review_category` (
  `review_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review_category`
--

INSERT INTO `review_category` (`review_id`, `category_id`) VALUES
(2, 2),
(2, 15),
(3, 1),
(3, 4),
(4, 2),
(4, 7),
(5, 7),
(5, 12),
(6, 1),
(6, 6),
(7, 2),
(7, 6),
(7, 15),
(7, 18),
(8, 2),
(8, 13),
(9, 2),
(9, 6),
(10, 12),
(10, 15),
(11, 4),
(11, 15),
(12, 2),
(12, 6),
(12, 15),
(12, 18);

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `moderator_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `moderator_id`, `email`) VALUES
(1, 4, 'user2@gmail.com'),
(7, 9, 'greisa.ajdini@aiesec.net'),
(9, 9, 'user111@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blocked` tinyint(1) NOT NULL,
  `bio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `first_name`, `last_name`, `blocked`, `bio`, `avatar`, `google_id`, `google_access_token`, `deleted_at`) VALUES
(3, 'admin', 'admin', 'admin@gmail.com', 'admin@gmail.com', 1, NULL, '$2y$13$INbqsDQ0T49Bg6S/U.7wyuzsQs5tjDoIfx2IT0i2QDCctBUOxCG/.', '2019-02-28 12:42:06', 'aFu7L33DZwQfHeEmsFWeBdqMecJJAx4pmXq-yZGo-R8', '2019-02-22 09:57:49', 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 'Admin', 'Admin', 0, NULL, NULL, NULL, NULL, NULL),
(4, 'grace97', 'grace97', 'greisa.ajdini@gmail.com', 'greisa.ajdini@gmail.com', 1, NULL, '108014546819378050463', '2019-02-28 12:29:55', 'aFoSxzXM__Xxi8JMXpnNSgjSJREVCsS_jhJkMrepqMY', '2019-02-22 09:47:57', 'a:1:{i:0;s:14:\"ROLE_MODERATOR\";}', 'Greisa', 'Ajdini', 0, 'I\'m 21 and I\'ve studied Computer Engineering. Watching movies is a hobby of mine so I created this blog to post my reviews.', '1ab05b8cacd309b0298e5a878b79421c.jpeg', '108014546819378050463', 'ya29.Gly-BpvMfwKyXkl3txvGUySPu5W163i9-dJjfeXrE3Hx6liR0QrSuTbBNNVMP9eNVkNWpuH2Yujvm3hczZQGIp0l7tizS6XdH8QM_FK6hLRAbQrYRd2SqwWes8a0Dg', NULL),
(5, 'user1', 'user1', 'gajdini15@epoka.edu.al', 'gajdini15@epoka.edu.al', 1, NULL, '$2y$13$ZHyrn9gwpaYVhznXlpoHjugzNp1OjHnZ6/vynI3lytwx6XotikFzG', '2019-02-28 16:20:07', NULL, NULL, 'a:0:{}', 'John', 'User', 0, NULL, NULL, NULL, NULL, NULL),
(6, 'user2', 'user2', 'user2@gmail.com', 'user2@gmail.com', 1, NULL, '$2y$13$D8x9uoAdWFqwjY9FChCHBuytzCGgxREB4qMcIloBml9qMyXcFg24O', '2019-02-26 23:20:21', 'S51G1P69yrQqLCw0dkttrX_f4RQ50lh9IEYwEXOSnwg', '2019-02-21 16:19:24', 'a:0:{}', 'Jenny', 'User', 1, NULL, NULL, NULL, NULL, NULL),
(7, 'user3', 'user3', 'user3@gmail.com', 'user3@gmail.com', 1, NULL, '$2y$13$KgfKgbMEeCnf0kN0ng1f.uoNHnYhKbmbekiXyw7m66cXnv8Q08XUe', '2019-02-21 15:21:48', NULL, NULL, 'a:0:{}', 'Jack', 'User', 0, NULL, NULL, NULL, NULL, NULL),
(8, 'user4', 'user4', 'user4@gmail.com', 'user4@gmail.com', 1, NULL, '$2y$13$cHSJqJ3gPPwLYqvJIbnAF.GMRi0CwOZCMOPskLwKF7EpHBdDWEXKG', '2019-02-26 23:30:28', NULL, NULL, 'a:0:{}', 'Brad', 'Sean', 0, NULL, NULL, NULL, NULL, '2019-02-04 16:00:00'),
(9, 'moderator2', 'moderator2', 'moderator2@gmail.com', 'moderator2@gmail.com', 1, NULL, '$2y$13$bWWujw4p7z7IDsqF4R75uO/UucOsQN8FA47KqMcCCRwDzOkaHyyui', '2019-02-27 15:19:57', NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_MODERATOR\";}', 'Mod', 'Moderator', 0, 'yayy', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`about`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_64C19C15E237E06` (`name`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526C3E2E969B` (`review_id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`moderator_id`,`follower_id`),
  ADD KEY `IDX_68344470D0AFA354` (`moderator_id`),
  ADD KEY `IDX_68344470AC24F853` (`follower_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`review_id`,`user_id`),
  ADD KEY `IDX_49CA4E7D3E2E969B` (`review_id`),
  ADD KEY `IDX_49CA4E7DA76ED395` (`user_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FF624B39D` (`sender_id`),
  ADD KEY `IDX_B6BD307FCD53EDB6` (`receiver_id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_794381C6D0AFA354` (`moderator_id`);

--
-- Indexes for table `review_category`
--
ALTER TABLE `review_category`
  ADD PRIMARY KEY (`review_id`,`category_id`),
  ADD KEY `IDX_250E90CA3E2E969B` (`review_id`),
  ADD KEY `IDX_250E90CA12469DE2` (`category_id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A3C664D3D0AFA354` (`moderator_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C3E2E969B` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `follow`
--
ALTER TABLE `follow`
  ADD CONSTRAINT `FK_68344470AC24F853` FOREIGN KEY (`follower_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_68344470D0AFA354` FOREIGN KEY (`moderator_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `FK_49CA4E7D3E2E969B` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_49CA4E7DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307FCD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_B6BD307FF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `FK_794381C6D0AFA354` FOREIGN KEY (`moderator_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `review_category`
--
ALTER TABLE `review_category`
  ADD CONSTRAINT `FK_250E90CA12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_250E90CA3E2E969B` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `FK_A3C664D3D0AFA354` FOREIGN KEY (`moderator_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
